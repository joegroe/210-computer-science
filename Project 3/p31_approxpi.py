'''
Art Show.
CIS 210 F17 Project 3

Author: Joseph Gregory

Credits: N/A

Monte Carlo algorithm to find the approximation of pi.
'''
import random, math
def montePi(numDarts):
    '''
    (numDarts) -> float

    Returns approximation of pi. takes numDarts as the number of iterations to approximate
    pi using Monte Carlo algorithm.
    Each output will range and tests will not be the same every time

    EXAMPLES:
    >>> montePi(100)
    3.08
    >>> montePi(100000)
    3.143072
    >>> montePi(10000000)
    3.1418752
    '''
    inCircle = 0

    for i in range(numDarts):
        y = random.random()
        x = random.random()
        d = math.sqrt(x**2 + y**2)
        isInCircle(x,y,1)   # radius is set to 1 since the entire square that the circle is in
                            # is 2 units wide and 2 units long

        if d <= 1:
            inCircle = inCircle +1

    pi = inCircle/numDarts * 4

    return pi

def isInCircle(x,y,r):
    '''
    (x,y,r) -> boolean

    returns true or false if the point is inside the circle from origin (0,0)

    EXAMPLES:
    >>> isInCircle(0, 0, 1)
    True
    >>> isInCircle(.5, .5, 1)
    True
    >>> isInCircle(1, 2, 1)
    False
    '''
    if x + y <= r or x and y <= r:
        return True
    else:
        return False

print(montePi(100))
print(isInCircle(1,1,1))