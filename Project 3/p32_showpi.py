'''
Approximating pi.  CIS 210 F17 Project 3-2
Starter Code for showMontePi

Author: [CIS 210 F17]

Credits: Based on code on p.78 Miller and Ranum text.

Approximate pi using a Monte Carlo simulation.
Then add graphics to visualize simulation.
'''

from turtle import *
import math
import random


def showMontePi(numDarts):
    '''
    (int) -> float

    Returns approximation of pi using Monte Carlo algorithm.
    Starts with setting up a canvas and turtle.
    iterates the number of dart throws and plots them on canvas.
    Compares the approximate pi to math lib pi and shows the percentage error between the two,
     assuming math lib pi is correct.

    tests may show different outcomes but close to approximation, due to math.random
    EXAMPLES:
    >>> showMontePi(100)
    3.14
    '''
    # set up canvas and turtle
    # to animate the algorithm;
    # draw x, y axes

    wn = Screen()
    wn.setworldcoordinates(-2, -2, 2, 2)

    speed(0);
    hideturtle()
    penup()

    goto(-1, 0)
    pendown()
    goto(1, 0)
    penup()
    goto(0, 1)
    pendown()
    goto(0, -1)
    penup()
    goto(0, -1)

    # pen should stay up for drawing darts

    inCircleCt = 0

    # throw the darts and check whether
    # they landed on the dart board and
    # keep count of those that do
    for i in range(numDarts):
        x = random.random()
        y = random.random()
        isInCircle(x, y, 1)

        # Revise code to call new isInCircle function.
        # See Project 3-1 and 3-2 specifications.

        d = math.sqrt(x ** 2 + y ** 2)

        # show the dart on the board
        if d < 1:
            inCircleCt += 1
            color('blue')
        else:
            color('red')

        goto(x, y)
        dot()

    # calculate approximate pi
    approxPi = inCircleCt / numDarts * 4

    math_pi = math.pi
    result = round((abs((math.pi - approxPi) / math.pi) * 100), 2)
    print("With {} iterations: \n"
          "my approximate value of pi is: {} \n"
          "math lib pi value is: {} \n"
          "This is a {} percent error.".format(numDarts, approxPi, math.pi, result))

    wn.exitonclick()

    return approxPi

def isInCircle(x,y,r):
    '''
    (x,y,r) -> boolean

    returns true or false if the point is inside the circle from origin (0,0)

    EXAMPLES:
    >>> isInCircle(0, 0, 1)
    True
    >>> isInCircle(.5, .5, 1)
    True
    >>> isInCircle(1, 2, 1)
    False
    '''
    if x + y <= r or x and y <= r:
        return True
    else:
        return False

print(showMontePi(1000))