'''
CIS 210 STYLE
CIS 210 F17 Project 1

Author: Joseph Gregory

Credits: N/A

Add docstrings to Python functions implementing quiz 1 pseudocode.
'''


def q1(onTime, absent):
    '''
    (boolean,boolean) -> None

    Prints 'Hello!' if onTime is true or prints 'Is Anyone there?' if absent is true.
    Prints 'Better late than never' if both are false.
    None value is returned.

    >>> q1(True, True)
    Hello!
    >>> q1(True,False)
    Hello!
    >>> q1(False, True)
    Is anyone there?
    >>> q1(False, False)
    Better late than never.
    '''
    if onTime:
        print('Hello!')
    elif absent:
        print('Is anyone there?')
    else:
        print('Better late than never.')

    return None


# q1(False, False)

def q2(age, salary):
    '''
    int(int,int) -> boolean
    returns true if both the age a dependent child is less than 18
    AND if they earn less than $10,000 a year

    >>> q2(17, 9999)
    True
    >>> q2(17,10000)
    False
    >>> q2(18, 9999)
    False
    '''
    return (age < 18) and (salary < 10000)


# print(q2(18, 5000))
# print(q2(16, 5000))

def q3():
    '''
    () -> int

    No argument given.
    if p is less than q, the function checks to see if q is greater than 4, otherwise result is returned.
    if q is greater than 4, result takes the value of 5 and is returned.
    If q is not greater than 4, result takes the value of 6 and is returned

    int value is returned

    >>> q3()
    6
    '''
    p = 1
    q = 2
    result = 4
    if p < q:
        if q > 4:
            result = 5
        else:
            result = 6

    return result


# print(q3())

def q4(balance, deposit):
    '''
    (int,int) -> int

    While count is less than 10, the balance given is added to itself including the deposit.
    after the deposit has been added to the balance, count increments by 1.
    Once count reaches 10, the code stops and returns the final balance

    >>> q4(100,10)
    200
    >>> q4(200,10)
    300
    '''

    count = 0
    while count < 10:
        balance = balance + deposit
        count += 1

    return balance


# print(q4(100, 10))

def q5(nums):
    '''
    (list) -> int

    while integer i is less than the length of the list, the function checks
     if the index [i] is greater than or equal to 0. If it is, results increases by 1.
     returns result
     >>> q5([0,1,2,3,4,5])
     6
     >>> q5([0,-1,0,0])
     3
    '''
    result = 0
    i = 0
    while i < len(nums):
        if nums[i] >= 0:
            result += 1

        i += 1

    return result


# print(q5([0, 1, 2, 3, 4, 5]))
# print(q5([0, -1, 2, -3, 4, -5]))

def q6():
    '''
    () -> None

    while the integer i is less than 4, multiplies p by 2 and sets the new value to p
    Commented out a line of code which would make the loop infinate

    >>> q6()
    2-power is 16

    '''
    i = 0
    p = 1
    while i < 4:
        #i = 1 <- this makes the while loop infinate
        p = p * 2
        i += 1

    print('2-power is', p)
    return None

# q6()


