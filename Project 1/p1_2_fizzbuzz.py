'''
Fizzbuzz Number Game
CIS 210 F17 Project 1-2

Author: Joseph Gregory

Credits: N/A

Plays the interger game fizzbuzz
'''

def fb(n):
    '''(int) -> None

    Given a certain number (n), the function starts from 1 to n.
    for every number the loop goes through, if the number is divisible
    by 3, 'fizz' is printed. If the number is divisible by 5,
    'buzz' is printed. If the number is divisible by both 3 and 5,
    'fizzbuzz' is printed. Otherwise, the number is printed

    EXAMPLES:
    >>> fb(5)
    1
    2
    fizz
    4
    buzz
    Game Over!
    >>> fb(10)
    1
    2
    fizz
    4
    buzz
    fizz
    7
    8
    fizz
    buzz
    Game Over!
    '''
    for x in range(1,n+1):
        if x%5 ==0 and x%3 == 0:
            print('fizzbuzz')
        elif x%3 == 0:
            print('fizz')
        elif x%5 == 0:
            print('buzz')
        else:
            print(x)
    print('Game Over!')

if __name__ == '__main__':
    fb(10000)