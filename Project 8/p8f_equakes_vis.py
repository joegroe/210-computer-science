'''World-wide Earthquake Watch
CIS 210 F17 Final Project

Author: Joseph Gregory

Credits: N/A

Visualization of earthquake plots around the world.
'''
import turtle, random, math

def flinesCtr(f):
   ''' count and return number of lines in file'''

   with open(f) as ctf:
      for i, l in enumerate(ctf):
          print("i: " + str(i))
          print("l: " + str(l))
   num_lines = i + 1
   print("num_lines: " + str(num_lines))
   '''
   num_lines = sum(1 for line in open(fname))
   '''
   return num_lines

def readFile(filename):
    """
    (String) -> Dictionary

    Reads in a filename and opens the file name.
    Sorts through the contents and returns a dictionary.
    """
    datafile = open(filename, "r")
    contents = [x.split(",") for x in datafile.readlines()] #list comprehension
    datadict = {}
    key = 0
    magnitudes = []

    for aline in contents[1:]:
        key = key + 1
        lat = float(aline[1])
        lon = float(aline[2])

        #Extra Credit
        mag = float(aline[4])
        if mag not in magnitudes:               # uses new list with each magnitude to see the
            magnitudes.append(mag)              # range of magnitudes in the dataset
        datadict[key] = [lon,lat,mag]           #new attribute added to dict
    magnitudes.sort()                           # sorts the list to see range better (5.0 -> 8.2)
    datafile.close()

    return datadict

def euclidD(point1, point2):
    """
    (list,list) -> float

    Takes the points of 2 different lists and sees the differences between the two
    to return the euclidian distance

    >>> euclidD([-68.5172, -22.3793, 5.5],[22.7352, -53.241, 5.2])
    96.32987611665448
    >>> euclidD([-178.1433, -31.3373, 5.2],[153.9035, -6.0891, 5.1])
    333.0053287764026
    >>> euclidD([-178.1433, -31.3373, 5.2],[168.8741, -21.7307, 5.1])
    347.15034591127807
    """
    total = 0
    for index in range(len(point1[:2])):            #point1 is sliced to 2 so the last element
        diff = (point1[index] - point2[index]) ** 2 #(magnitude) is not counted in total
        total = total + diff

    euclidDistance = math.sqrt(total)
    return euclidDistance

def createCentroids(k, datadict):
    """
    (int, dictionary) -> list of lists

    Takes the k-means cluster by the integer given and goes through datadict to create the centroids through
     a random key in range of 1 to the length of datadict

    >>> createCentroids(3,{1: [129.2695, 36.0645, 5.4], 2: [-174.6136, -17.4576, 5.2], 3: [-17.1484, -59.0385, 5.3]})
    [[-17.1484, -59.0385, 5.3], [129.2695, 36.0645, 5.4], [-174.6136, -17.4576, 5.2]]
    >>> createCentroids(3,{1: [-171.6991, -18.9217, 5.6], 2: [-177.3966, -31.2848, 5.0], 3: [-177.9864, -18.1544, 5.5]})
    [[-177.3966, -31.2848, 5.0], [-171.6991, -18.9217, 5.6], [-177.9864, -18.1544, 5.5]]
    >>> createCentroids(3,{1: [73.9098, 38.2556, 5.0], 2: [129.9681, -7.0579, 5.0], 3: [-84.0888, -41.4839, 5.8]})
    [[129.9681, -7.0579, 5.0], [73.9098, 38.2556, 5.0], [-84.0888, -41.4839, 5.8]]
    """
    centroids = []
    centroidCount = 0
    centroidKeys = []

    while centroidCount < k:
        rkey = random.randint(1,len(datadict))
        if rkey not in centroidKeys:
            centroids.append(datadict[rkey])
            centroidKeys.append(rkey)
            centroidCount = centroidCount + 1
    return centroids

def createClusters(k,centroids,datadict,r):
    """
    (int, list, dictionary, int) -> list of lists

    Creates clusters by the number of k-clusters by the centroids created, datadict, and the number of repetitions
    and returns a list of the clusters

    >>> createClusters(3,[[-17.1484, -59.0385, 5.3], [129.2695, 36.0645, 5.4], [-174.6136, -17.4576, 5.2]],
                     {1: [129.2695, 36.0645, 5.4], 2: [-174.6136, -17.4576, 5.2], 3: [-17.1484, -59.0385, 5.3]},
                     2)
    [[3], [1], [2]]
    >>> createClusters(3,[[-177.3966, -31.2848, 5.0], [-171.6991, -18.9217, 5.6], [-177.9864, -18.1544, 5.5]],
                     {1: [-171.6991, -18.9217, 5.6], 2: [-177.3966, -31.2848, 5.0], 3: [-177.9864, -18.1544, 5.5]},
                     2)
    [[2], [1], [3]]
    >>> createClusters(3,[[129.9681, -7.0579, 5.0], [73.9098, 38.2556, 5.0], [-84.0888, -41.4839, 5.8]],
                     {1: [73.9098, 38.2556, 5.0], 2: [129.9681, -7.0579, 5.0], 3: [-84.0888, -41.4839, 5.8]},
                     2)
    [[2], [1], [3]]
    """
    for apass in range(r):
        #print("****PASS", apass,"****")
        clusters = []
        for i in range(k):
            clusters.append([])
        for akey in datadict:
            distances = []
            for clusterIndex in range(k):
                dist = euclidD(datadict[akey], centroids[clusterIndex])
                distances.append(dist)
            mindist = min(distances)
            index = distances.index(mindist)
            clusters[index].append(akey)

        dimensions = len(datadict[1])
        for clusterIndex in range(k):
            sums = [0]*dimensions
            for akey in clusters[clusterIndex]:
                datapoints = datadict[akey]
                for ind in range(len(datapoints)):
                    sums[ind] = sums[ind] + datapoints[ind]
            for ind in range(len(sums)):
                clusterLen = len(clusters[clusterIndex])
                if clusterLen != 0:
                    sums[ind] = sums[ind]/clusterLen
            centroids[clusterIndex] = sums

        #for c in clusters:
            #print("CLUSTER")
            #for key in c:
                #print(datadict[key], end = " ")
            #print()
    return clusters

def visualizeQuakes(k, r, dataFile):
    """
    (int,int,string) -> None

    Calls all the auxilary functions to prepare to draw a visualization of points

    >>> visualizeQuakes(3,2,'earthquakes.txt')
    None
    """
    datadict = readFile(dataFile)
    quakeCentroids = createCentroids(k, datadict)
    clusters = createClusters(k, quakeCentroids, datadict, r)
    eqDraw(k, datadict, clusters)
    return None

def eqDraw(k,eqDict,eqClusters):
    """
    (int, dictionary,list) -> None

    Creates turtle object and uses worldmap.gif as the background for the the visualization of the earthquake plots.

    > eqDraw(6, {1: [129.2695, 36.0645, 5.4], 2: [-174.6136, -17.4576, 5.2], 3: [-17.1484, -59.0385, 5.3]},
                [[3], [1], [2]])
    [visualization of worldmap with plots]
    """
    quakeT = turtle.Turtle()
    quakeWin = turtle.Screen()
    quakeWin.bgpic("worldmap.gif")
    quakeWin.screensize(1800,900)

    wFactor = (quakeWin.screensize()[0]/2) / 180
    hFactor = (quakeWin.screensize()[1]/2) / 90

    quakeT.hideturtle()
    quakeT.speed('fastest')
    quakeT.up()

    colorlist = ["red","green","blue","orange","cyan","yellow"]

    for clusterIndex in range(k):
        quakeT.color(colorlist[clusterIndex])
        for akey in eqClusters[clusterIndex]:
            lon = eqDict[akey][0]
            lat = eqDict[akey][1]
            mag = eqDict[akey][2]
            quakeT.goto(lon*wFactor, lat*hFactor)
            quakeT.dot(size = (mag*1.2)) # sets the size of each dot based on it's magnitude (times) 1.2
    quakeWin.exitonclick()

    return None

def main():
    """
    () -> None

    sets f,k and r values to call visualizeQuakes.

    """
    f = "earthquakes.txt"
    k = 6
    r = 7
    #visualizeQuakes(k,r,f)
    print(flinesCtr(f))

    return None

if __name__ == "__main__":
    main()

##----------------------------------------------------EXTRA CREDIT----------------------------------------------------##
import csv  #used to import dataset
def openCSVFile(filename):
    """
    (string) -> list

    Opens a csv file and returns a list of a dictionaries.
    """
    datalist = []
    key = 0

    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        contents = [row for row in reader]  #list comprehension
    csvfile.close()

    for item in contents[1:]:
        temp_dict = {"delays" : item[4],
                     "code number" : item[7],
                     "delayed month" : item[8],
                     "total flights" : item[10],
                     "month" : item[19],
                     "year" : item[21]}
        datalist.append(temp_dict)

    return datalist

def mode(alist):
    '''(list of numbers) -> list

    Return mode(s) of alist.

    Calls: genFreqTable

    >>> mode([5, 7, 1, 3])
    [1, 3, 5, 7]
    >>> mode([1, 2, 2, 3, 99])
    [2]
    >>> mode([99])
    [99]
    >>> mode([0, 0, 1, 1])
    [0, 1]
    '''
    countdict = {}

    for item in alist:
        if item in countdict:
            countdict[item] = countdict[item]+1
        else:
            countdict[item] = 1
    countlist = countdict.values()
    maxcount = max(countlist)

    modelist = []
    for item in countdict:
        if countdict[item] == maxcount:
            modelist.append(item)
    return modelist

def analyze_delays(datalist):
    """
    (list) -> None

    Reports the total flight delays, average delays per month, total flights, percent of flights without delay
    and the mode of most flights from a certain airline code.

    >>> analyze_delays({"delays":20, "code number":"ATL", "delayed month":800, "total flights":1000, "month":"June",
                    "year":2010},{"delays":40, "code number":"ATL", "delayed month":400, "total flights":500, "month":"June",
                    "year":2010},{"delays":55, "code number":"ATL", "delayed month":1000, "total flights":12000, "month":"June",
                    "year":2010})
    "Total flights delayed: 115
    Average delays per month: 38.333333333333336

    Total flights: 13500
    Flights made without delay: 13385
    Percent of flights made without delay: 99.1%
    Most flight(s) flown: ['ATL']"

    >>> analyze_delays({"delays":200, "code number":"ATL", "delayed month":800, "total flights":1000, "month":"June",
                    "year":2010},{"delays":40, "code number":"ATL", "delayed month":400, "total flights":500, "month":"June",
                    "year":2010},{"delays":55, "code number":"LAX", "delayed month":1000, "total flights":12000, "month":"June",
                    "year":2010})
    "Total flights delayed: 295
    Average delays per month: 98.33333333333333

    Total flights: 13500
    Flights made without delay: 13205
    Percent of flights made without delay: 97.8%
    Most flight(s) flown: ['ATL']"

    >>> analyze_delays({"delays":100, "code number":"ATL", "delayed month":990, "total flights":2000, "month":"June",
                    "year":2010},{"delays":40, "code number":"JFK", "delayed month":400, "total flights":500, "month":"June",
                    "year":2010},{"delays":55, "code number":"JFK", "delayed month":1000, "total flights":12000, "month":"June",
                    "year":2010})
    "Total flights delayed: 195
    Average delays per month: 65.0

    Total flights: 14500
    Flights made without delay: 14305
    Percent of flights made without delay: 98.7%
    Most flight(s) flown: ['JFK']"
    """
    total_sum = 0
    total_flights = 0
    codelist = []

    for dict in datalist:
        total_sum = total_sum + int(dict["delays"])
        total_flights = + total_flights + int(dict["total flights"])
        codelist.append(dict["code number"])

    average = total_sum / len(datalist)
    total_from_delayed = total_flights - total_sum
    percentage = round((total_from_delayed / total_flights),3)

    mode_code = mode(codelist)


    print("Total flights delayed: {}\n"
          "Average delays per month: {}\n\n"
          "Total flights: {}\n"
          "Flights made without delay: {}\n"
          "Percent of flights made without delay: {}%\n"
          "Most flight(s) flown: {}".format(total_sum, average,total_flights,
                                                total_from_delayed,(percentage*100),
                                                  mode_code))
    return None

def main2():
    """
    () -> None
    calls openCSVFile to analyze elements.
    """
    filename = openCSVFile('airlines.csv')
    analyze_delays(filename)

    return None


#if __name__ == "__main__":
    #main2()










