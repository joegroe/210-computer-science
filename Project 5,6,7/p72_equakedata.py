'''Earthquake Data Analysis Report
CIS 210 F17 Project 7

Author: Joseph Gregory

Credits: N/A

Reports the number of earthquakes of a certain magnitude and reports the mean, median and mode
along with a frequency occurance table.
'''
import p62_data_analysis as p6

def equake_readf(fname):
    """
    (string) -> list

    reads in a string name and uses it to open file with string name.
    returns a list of magnitudes in the file.
    """
    magnitudes = []
    with open(fname) as file:
        contents = [line.split(",") for line in file.readlines()]
    for magnitude in contents[1:]:
        magnitudes.append(float(magnitude[4]))
    return magnitudes

def equake_analysis(magnitudes):
    """
    (list) -> tuple

    returns a tuple of the mean, median, and mode of magnitudes.

    >>> equake_analysis([5.0,5.3,5.3,5.4,6.0])
    (5.4, 5.3, [5.3])

    >>> equake_analysis([3.0,4.0,7.2,4.0])
    (4.55, 4.0, [4.0])

    >>> equake_analysis([3.0,2.0,5.0,9.0,9.0,5.0])
    (5.5, 5.0, [9.0, 5.0])
    """
    mag_mean = p6.mean(magnitudes)
    mag_median = p6.median(magnitudes)
    mag_mode = p6.mode(magnitudes)
    return (mag_mean,mag_median,mag_mode)

def equake_report(magnitudes, mmm):
    """
    (list,tuple) -> None
    reports the mean, median, and mode of the magnitudes of
    earthquakes. Returns non

    > equake_report([5.0,5.3,5.3,5.4,6.0], (5.4, 5.3, [5.3]))
        Earthquake Data Analysis
        25 Years Ago to Present
        250km centered at Eugene,OR

    There have been 1 earthquakes over the past 25 years.

    Mean magnitude is: 5.4
    Median magnitude is: 5.3
    Mode(s) of magnitudes is: [5.3]

    [frequency occurrences chart]

    """
    mag_mean, mag_median, mag_mode = mmm
    print(mmm)
    print("\t Earthquake Data Analysis\n"
          "\t25 Years Ago to Present\n"
          "\t250km centered at Eugene,OR\n\n"
          "There have been {} earthquakes over the past 25 years.\n\n"
          "Mean magnitude is: {}\n"
          "Median magnitude is: {}\n"
          "Mode(s) of magnitudes is: {}\n".format(len(magnitudes),mag_mean,mag_median,mag_mode))
    p6.frequencyTable(magnitudes)
    return None

print(equake_report([3.0,4.0,7.2,4.0], (5.4, 5.3, [5.3])))

def main():
     '''()-> None
     Calls: equake_readf, equake_analysis, equake_report
     Top level function for earthquake data analysis.
     Returns None.
     '''
     fname = 'equake25f.txt'
     #fname = 'equake50f.txt'

     emags = equake_readf(fname)
     mmm = equake_analysis(emags)
     equake_report(emags, mmm)
     return None

if __name__ == "__main__":
    main()

import doctest
print(doctest.testmod())