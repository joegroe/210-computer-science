'''Testing Functions
CIS 210 F17 Project 6

Author: Joseph Gregory

Credits: N/A

creating and implementing a function to automate testing
'''
import p52_stringreverse as p5

def test_reverse(f):
    test_cases = ((('', ''),('a', 'a'),('xyz', 'zyx'),('testing123', '321gnitset'),('hello, world', 'dlrow ,olleh')))
    for test in test_cases:
        f_result = f(test[0])
        if f_result == test[1]:
            print("Checking({}) ... its value {} is correct!".format(test[0],test[1]))
        else:
            print("Checking({}) ... Error: has wrong value {}, expected {}".format(test[0],f(test[0]),test[1]))

def is_member(set,number):  ##Extra Credit

    midpoint = len(set)//2
    if len(set) == 0:
        return False
    else:
        if number == set[midpoint]:
            return True
        else:
            if number < set[midpoint]:
                return is_member(set[:midpoint],number)
            else:
                return is_member(set[midpoint+1:],number)

def main():
    '''calls string reverse test func 2x'''
    test_reverse(p5.strReverseR)
    test_reverse(p5.strReverseI)
    return None

if __name__ == "__main__":
    main()
