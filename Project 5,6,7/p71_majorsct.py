'''CIS 210 Data Analyis Report
CIS 210 F17 Project 7

Author: Joseph Gregory

Credits: N/A

Reports the mode for CIS 210 majors and frequency occurance chart
'''
import p62_data_analysis as p6

def majors_readf(fname):
    '''
    (string) -> list

    reads in a string name and uses it to open file with string name.
    returns a list of magnitudes in the file.

    > majors_readf('demo.txt')
    [list of items]
    '''
    majorsli = []
    with open(fname) as file:
        contents = [line.split(" ") for line in file.readlines()]
    for major in contents[1:]:
        majorsli.append(major[0])
    return majorsli

def majors_analysis(majorsli):
    """
    (list) -> List

    returns the mode of majorsli

    # RESULTS MAY VARY DUE TO LISTS NOT BEING SORTED

    >>> majors_analysis(['CIS','CIS','MATH','BA'])
    ['CIS']

    >>> majors_analysis(['CIS','CIS','MATH','MATH','PSY'])
    ['MATH', 'CIS']

    >>> majors_analysis(['CIS','MATH','PSY','BA'])
    ['PSY', 'MATH', 'BA', 'CIS']
    """
    majors_mode = p6.mode(majorsli)
    return majors_mode

def majors_report(majors_mode, majorsli):
    """
    (list,list) -> None

    Reports mode of majors and calls frequencyTable from p6

    > majors_report(['CIS'],['CIS','CIS','MATH','BA'])
    Most represented major(s):
    ['CIS']

    [frequency table occurances]
    """
    print("Most represented major(s):\n{}\n".format(str(majors_mode)))
    p6.frequencyTable(majorsli)
    return None

def main():
    '''()-> None
    Calls: majors_readf, majors_analysis, majors_report
    Top level function for analysis of CIS 210 majors data.
    Returns None.
    > majors_main()
    '''
    fname = 'majors_cis210f17.txt'

    majorsli = majors_readf(fname)
    majors_mode = majors_analysis(majorsli)
    majors_report(majors_mode, majorsli)
    return None

if __name__ == "__main__":
    main()
    #import doctest
    #doctest.testmod()

