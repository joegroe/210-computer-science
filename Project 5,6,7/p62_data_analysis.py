'''Testing Functions
CIS 210 F17 Project 6

Author: Joseph Gregory

Credits: N/A

creating and implementing a function to automate testing
'''
def genFrequencyTable(alist):
    '''(list of numbers) -> dictionary

        Generate a frequency dictionary with
        number of occurrences of each number
        in alist.

        Called by:  freqTable, freqChart, mode

        > genFreqTable([1, 2, 3, 3, 1, 4, 5])
        {1:2, 2:1, 3:2, 4:1, 5:1}
        '''
    dict_frequency = {}

    for item in alist:
        if item in dict_frequency:
            dict_frequency[item] += 1
        else:
            dict_frequency[item] = 1
    return dict_frequency

def frequencyTable(alist):
    '''(list of numbers) -> None

        Print frequency table of count
        of each number in alist.
        None value is returned.

        Calls:  genFreqTable, drawTable

        > freqTable([1, 2, 3, 3, 1, 4, 5])
        [frequency occurrences chart]
        '''
    countdict = {}

    for item in alist:
        if item in countdict:
            countdict[item] = countdict[item] + 1
        else:
            countdict[item] = 1
    itemlist = list(countdict.keys())
    itemlist.sort()

    print("ITEM","FREQUENCY")
    for item in itemlist:
        print(item, "\t",countdict[item])

def isEven(n):
    '''(num) -> boolean

    Return True if n is even,
    else return False

    >>> even(4)
    True
    >>> even(1)
    False
    >>> even(0)
    True
    '''
    return (n % 2) == 0

def mean(alist):
    '''(list of numbers) -> number

    Return median of alist (of len > 0).

    >>> median([5, 7, 1, 3])
    4.0
    >>> median([1, 2, 2, 3, 99])
    2
    >>> median([99])
    99
    >>> median([0, 0, 0, 0])
    0.0
    '''
    mean = sum(alist) / len(alist)
    return mean

def median(alist):
    copylist = alist[:]
    copylist.sort()
    if isEven(len(copylist)):    #isEven function call
        rightmid = len(copylist) // 2
        leftmid = rightmid - 1
        median = (copylist[leftmid] + copylist[rightmid]) / 2
    else:
        mid = len(copylist) // 2
        median = copylist[mid]
    return median

def mode(alist):
    '''(list of numbers) -> list

    Return mode(s) of alist.

    Calls: genFreqTable

    >>> mode([5, 7, 1, 3])
    [1, 3, 5, 7]
    >>> mode([1, 2, 2, 3, 99])
    [2]
    >>> mode([99])
    [99]
    >>> mode([0, 0, 1, 1])
    [0, 1]
    '''
    countdict = {}

    for item in alist:
        if item in countdict:
            countdict[item] = countdict[item]+1
        else:
            countdict[item] = 1
    countlist = countdict.values()
    maxcount = max(countlist)

    modelist = []
    for item in countdict:
        if countdict[item] == maxcount:
            modelist.append(item)
    return modelist

import turtle

def frequencyChart(alist):
    countdict = {}

    for item in alist:
        if item in countdict:
            countdict[item] = countdict[item] + 1
        else:
            countdict[item] = 1

    itemlist = list(countdict.keys())
    minitem = 0
    maxitem = len(itemlist)-1

    countlist = countdict.values()
    maxcount = max(countlist)
    drawChart(countdict,maxitem,maxcount,itemlist)

def drawChart(countdict, maxitem,maxcount,itemlist):
    wn = turtle.Screen()
    chartT = turtle.Turtle()
    wn.setworldcoordinates(-1,-1,maxitem+1,maxcount+1)
    chartT.hideturtle()

    chartT.up()
    chartT.goto(0,0)
    chartT.down()
    chartT.goto(maxitem,0)
    chartT.up()

    chartT.goto(-1,0)
    chartT.write("0",font = ("Helvetica",16,"bold"))
    chartT.write(str(maxcount),font =("Helvetica",16,"bold"))

    for index in range(len(itemlist)):
        chartT.goto(index,-1)
        chartT.write(str(itemlist[index]),font=("Helvetica",16,"bold"))

        chartT.goto(index,0)
        chartT.down()
        chartT.goto(index,countdict[itemlist[index]])
        chartT.up()
    wn.exitonclick()


def main():
    equakes = [5.3, 3.0, 2.6, 4.4, 2.9, 4.8, 4.3, 2.6, 2.9, 4.9, 2.5, 4.8, 4.2, 2.6, 4.8, 2.7, 5.0, 2.7, 2.8, 4.3, 3.1,
    4.1, 2.8, 5.8, 2.5, 3.9, 4.8, 2.9,2.5, 4.9, 5.0, 2.5, 3.2, 2.6, 2.7,4.8, 4.1, 5.1, 4.7, 2.6, 2.9, 2.7,
    3.3, 3.0, 4.4, 2.7, 5.7, 2.5, 5.1,2.5, 4.4, 4.6, 5.7, 4.5, 4.7, 5.1,2.9, 3.3, 2.7, 2.8, 2.9, 2.6, 5.3,
    6.0, 3.0, 5.3, 2.7, 4.3, 5.4, 4.4,2.6, 2.8, 4.4, 4.3, 4.7, 3.3, 4.0,2.5, 4.9, 4.9, 2.5, 4.8, 3.1, 4.9,
    4.4, 6.6, 3.3, 2.5, 5.0, 4.8, 2.5,4.2, 4.5, 2.6, 4.0, 3.3, 3.1, 2.6,2.7, 2.9, 2.7, 2.9, 3.3, 2.8, 3.1,
    2.5, 4.3, 3.2, 4.6, 2.8, 4.8, 5.1,2.7, 2.6, 3.1, 2.9, 4.2, 4.8, 2.5,4.5, 4.5, 2.8, 4.7, 4.6, 4.6, 5.1,
    4.2, 2.8, 2.5, 4.5, 4.6, 2.6, 5.0,2.8, 2.9, 2.7, 3.1, 2.6, 2.5, 3.2,3.2, 5.2, 2.8, 3.2, 2.6, 5.3, 5.5,
    2.7, 5.2, 6.4, 4.2, 3.1, 2.8, 4.5,2.9, 3.1, 4.3, 4.9, 5.2, 2.6, 6.7,2.7, 4.9, 3.0, 4.9, 4.7, 2.6, 4.6,
    2.5, 3.2, 2.7, 6.2, 4.0, 4.6, 4.9,2.5, 5.1, 3.3, 2.5, 4.7, 2.5, 4.1,3.1, 4.6, 2.8, 3.1, 6.3]

    print(genFrequencyTable(equakes))
    print("mode" + str(mode(equakes)))
    print("median" + str(median(equakes)))
    frequencyChart(equakes)
if __name__ == "__main__":
    main()