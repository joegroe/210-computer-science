'''
Binary Encoding and Decoding Recursion
CIS 210 F17 Project 5

Author: Joseph Gregory

Credits: N/A

Converts a non-negative decimal into binary and the binary form back to decimal.
Contains both recursive and non-recursive for decimal to binary functions.
'''
def dtobr(n):   ## Recursive
    '''
    (int) -> String

    converts an interger into binary recursively

    >>> dtobr(27)
    '11011'
    >>> dtobr(0)
    '0'
    >>> dtobr(1)
    '1'
    >>> dtobr(2)
    '10'
    >>> dtobr(44)
    '101100'
    '''
    dtob = ""
    if n < 2 :
        return str(n)
    else:
        dtob = dtob + dtobr(n//2) + str(n%2)
        return dtob

def dtob(n):    ## non-recursive
    '''
    (int) -> String

    converts an integer into it's respected binary form of base 2.
    returns a binary string

    >>> dtob(27)
    '11011'
    >>> dtob(0)
    '0'
    >>> dtob(1)
    '1'
    >>> dtob(2)
    '10'
    '''
    dtob = ''
    if n == 0:
        return '0'
    while n > 0:
        temp = n % 2
        dtob = dtob + str(temp)
        n = n//2
    return dtob[::-1]

def btod(b):
    '''
    (String) -> int

    converts a string of integers in binary form into a decimal of base 10.
    returns a base 10 decimal as an integer.

    >>> btod('0000')
    0
    >>> btod('1101')
    13
    >>> btod('111111')
    63

    '''
    decimal = 0
    for n in b:
        decimal = decimal * 2 + int(n)
    return decimal

def main():
    '''
    () -> None

    Calls three functions. One to convert decimal integer to binary through iteration and recursive.
    if both the outputs are the same, then calls the next function to convert binary to decimal
    '''
    inp = input("please enter a non-negative decimal integer: \n")
    if not int(inp) >= 0:
        raise ValueError("n must be >= 0")
    dtob_n = int(inp)
    if dtob(dtob_n) == dtobr(dtob_n):
        print(dtob(dtob_n))
        btod_b = dtob(dtob_n)
        print(btod(btod_b))


if "__main__" == __name__:
    import doctest
    doctest.testmod()