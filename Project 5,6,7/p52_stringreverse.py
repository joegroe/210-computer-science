'''String Reversal
CIS 210 F17 Project 5

Author: Joseph Gregory

Credits: N/A

Takes a string and returns the reverse of the string
'''

def strReverseR(s):
    '''
    (String) -> String

    Takes a string and returns it reverse rescursively.

    >>> strReverseR("hello world")
    'dlrow olleh'
    >>> strReverseR("whats up?")
    '?pu stahw'
    >>> strReverseR("abcdefghijklmnopqrstuvwxyz")
    'zyxwvutsrqponmlkjihgfedcba'
    '''
    if s == "":
        return s
    else:
        return s[-1] + strReverseR(s[:-1])

def strReverseI(s):
    '''
    (String) -> String
    Takes a string and returns it reverse non-recursively.

    >>> strReverseR("hello world")
    'dlrow olleh'
    >>> strReverseR("whats up?")
    '?pu stahw'
    >>> strReverseR("abcdefghijklmnopqrstuvwxyz")
    'zyxwvutsrqponmlkjihgfedcba'
    '''
    new_string = ""
    for x in s:
        new_string = x + new_string
    return new_string

def main():
    '''
    () ->

    Takes input from user, reverses it, then prints the reversal of the reversal :)
    '''
    s = input("Type a phrase you would like reversed: \n")
    print(strReverseR(strReverseI(s)))


if '__main__' == __name__:
    import doctest
    doctest.testmod()