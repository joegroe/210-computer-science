'''
Substitution Cipher
CIS 210 F17 Project 4

Author: Joseph Gregory

Credits: N/A

encrypts a plain text into a cipher text then decrypts the cipher text into plain text.
'''

def main():
    '''
    () -> None

    Allows user input to receive the string to encrypt and
    the password for the encryption.
    Calls two functions, one to encrypt the string and
    create the password and
    one to decrypt the cipher text into plain text using
    the same password.


    >>> main() --> [USING ('the quick brown fox', 'ajax')]
    Input string to encrypt: Input password: qdznrexgjoltkblu
    thequickbrownfox
    '''
    plain_text = input("Input string to encrypt: ")
    psw = input("Input password: ")

    cipher_text = substitutionEncrypt(plain_text, psw)
    print(substitutionEncrypt(plain_text,psw))
    print(substitutionDecrypt(cipher_text,psw))

def substitutionEncrypt(plain_text,psw):
    '''
    (String, String) -> String

    Encrypts a cipher text from a plain text and calls
    another function to create a passsword from the
    password parameter provided.
    Returns the cipher text from the plain text

    >>> substitutionEncrypt("the quick brown fox","ajax")
    'qdznrexgjoltkblu'

    >>> substitutionEncrypt("the quick brown fox", 'acdf')
    'vjgswkdmctqyphqz'

    >>> substitutionEncrypt('the quick brown fox', 'fpafz')
    'sebotgaipqmvlcmw'

    '''
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    plain_text = plain_text.lower().replace(" ", "")
    cipher_text  = ""
    cipher_key = genKeyFromPass(psw)
    for ch in plain_text:
        idx = alphabet.find(ch)
        cipher_text = cipher_text + cipher_key[idx]
    return cipher_text

def substitutionDecrypt(cipher_text,psw):
    '''
    (String, String) -> String

    decrypts a cipher text into a plain text and calls
    another function to create a passsword from the
    password parameter provided.
    Returns the plain text from the cipher

    >>> substitutionDecrypt('qdznrexgjoltkblu', 'ajax')
    'thequickbrownfox'

    >>> substitutionDecrypt('vjgswkdmctqyphqz', 'acdf')
    'thequickbrownfox'

    >>> substitutionDecrypt('sebotgaipqmvlcmw', 'fpafz')
    'thequickbrownfox'
    '''
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    plain_text = ""
    cipher_key = genKeyFromPass(psw)
    for ch in cipher_text:
        idx = cipher_key.find(ch)
        plain_text = plain_text + alphabet[idx]
    return plain_text

def genKeyFromPass(password):
    '''
    (String) -> String

    Takes a password and creates a key for the cipher functions to encrypt
    and decrypt and returns the key

    >>> genKeyFromPass('ajax')
    'ajxyzbcdefghiklmnopqrstuvw'

    >>> genKeyFromPass('adcf')
    'adcfghijklmnopqrstuvwxyzbe'

    >>> genKeyFromPass('afab')
    'afbcdeghijklmnopqrstuvwxyz'
    '''
    key = 'abcdefghijklmnopqrstuvwxyz'
    password = removeDupes(password)
    last_char = password[-1]
    last_idx = key.find(last_char)
    after_string = removeMatches(key[last_idx + 1:], password)
    before_string = removeMatches(key[:last_idx],password)
    key = password + after_string + before_string
    return key

def removeDupes(my_string):
    '''
    (String) -> String

    Removes duplicate characters from a string and returns
    a new string without duplicate characters

    >>> removeDupes('ajax')
    'ajx'

    >>> removeDupes('abab')
    'ab'

    >>> removeDupes('azbc')
    'azbc'
    '''
    new_str = ""
    for ch in my_string:
        if ch not in new_str:
            new_str = new_str + ch
    return new_str

def removeMatches(my_string,remove_string):
    '''
    (String,String) -> String

    removes all characters from my_string that are in remove_string.
    Returns the new string created.
    '''
    new_str = ""
    for ch in my_string:
        if ch not in remove_string:
            new_str = new_str + ch
    return new_str

if __name__ == "__main__":
    main()
    #import doctest
    #doctest.testmod()
