'''
Binary Encoding and Decoding
CIS 210 F17 Project 4

Author: Joseph Gregory

Credits: N/A

Converts a non-negative decimal into binary recursively and non-recursively, then binary form back to decimal.
'''
def main():
    '''
    () -> None

    Calls three functions. One to convert decimal integer to binary through iteration and recursive.
    if both the outputs are the same, then calls the next function to convert binary to decimal

    >>> main()
    please enter a non-negative decimal integer:
    101
    5
    >>> main()
    please enter a non-negative decimal integer:
    11001
    25
    >>> main()
    please enter a non-negative decimal integer:
    1010011
    83

    '''
    inp = input("please enter a non-negative decimal integer: ")
    dtob_n = int(inp)
    print(dtob(dtob_n))
    btod_b = dtob(dtob_n)
    print(btod(btod_b))

def dtob(n):
    '''
    (int) -> String

    converts an integer into it's respected binary form of base 2.
    returns a binary string

    >>> dtob(27)
    '11011'
    >>> dtob(0)
    '0'
    >>> dtob(1)
    '1'
    >>> dtob(2)
    '10'
    '''
    dtob = ''
    if n == 0:
        return '0'
    while n > 0:
        temp = n % 2
        dtob = dtob + str(temp)
        n = n//2
    return dtob[::-1]

def btod(b):
    '''
    (String) -> int

    converts a string of integers in binary form into a decimal of base 10.
    returns a base 10 decimal as an integer.

    >>> btod('0000')
    0
    >>> btod('1101')
    13
    >>> btod('111111')
    63

    '''
    decimal = 0
    for n in b:
        decimal = decimal * 2 + int(n)
    return decimal

if __name__ == "__main__":
    main()
    #import doctest
    #doctest.testmod()