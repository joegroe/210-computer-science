'''
Art Show.
CIS 210 F17 Project 2

Author: Joseph Gregory

Credits: https://docs.python.org/2/library/random.html

Uses Turtle to draw an art show. In this case code draws 2 geometric flowers, one inside
of another one.
'''
from turtle import *
import random


def draw_hexigon(turtle_obj,side_length):
    '''
    (obj, int) -> None
    :param turtle_obj:
    :param side_length:
    :return:
    '''
    for i in range(6):
        turtle_obj.fd(side_length)
        turtle_obj.right(60)
    return None

def draw_square(turtle_obj,side_length):
    '''
    (obj,int) -> None

    Returns None. Draw's a square using the turtle object.
    '''
    for i in range(4):
        turtle_obj.fd(side_length)
        turtle_obj.right(90)
    return None

def select_color():
    '''
    () -> String

    Returns color. Selects a random choice from a list of colors
    '''
    colors = ['black','blue','purple']
    color = random.choice(colors)   #imported random library to select random choice in list
    return color                    #found from sourse: https://docs.python.org/2/library/random.html

def art_show(shape_num,side_length):
    '''
    (int,int) -> None

    Returns None. Takes the number of shapes used
    and the side length to create 2 different shapes.
    First the function uses Turtle to draw a geometric flower using
    hexigons, then in the inner circle uses squares, to create one giant geometric flower.
    '''
    turtle_obj = Turtle()
    turtle_obj.width(8)
    color = select_color()
    turtle_obj.color(color)
    for i in range(shape_num):
        turtle_obj.right(360/shape_num)
        draw_hexigon(turtle_obj,side_length)
    for j in range(shape_num):
        draw_square(turtle_obj,side_length)
        turtle_obj.right(360 / shape_num)
    return None

#if __name__ == '__main__':
    #art_show(25,180)