'''
Approximate Square Root.
CIS 210 F17 Project 2

Author: Joseph Gregory

Credits: N/A

Approximate the square root of a number.
'''

from math import *

def mysqrt(n,k):
    '''
    (int,int) -> float

    Returns the approximation of the square root of a number, n using k iterations.
    >>> mysqrt(25, 5)
    5.000023178253949
    '''
    x_k = 1
    x_kp = " "
    for i in range(k):
        x_kp = (1/2)*(x_k + (n/x_k))
        x_k = x_kp
    return x_kp

def sqrt_compare(num, iterations):
    '''
    (int,int) -> float

    returns None. Takes the return value from mysqrt()
    and compares it to the math lib result.

    >>> sqrt_compare(10000, 8)
    For 10000 using 8 iterations:
    mysqrt value is: 101.20218365353946
    math lib sqrt value is: 100.0
    This is a 1.2 percent error.

    >>> sqrt_compare(25,5)
    For 25 using 5 iterations:
    mysqrt value is: 5.000023178253949
    math lib sqrt value is: 5.0
    This is a 0.0 percent error.

    >>> sqrt_compare(100,10)
    For 100 using 10 iterations:
    mysqrt value is: 10.0
    math lib sqrt value is: 10.0
    This is a 0.0 percent error.

    '''
    my_sqrt = mysqrt(num,iterations)
    math_sqrt = sqrt(num)
    result = round((abs((math_sqrt - my_sqrt)/math_sqrt) *100), 2)
    print("For {} using {} iterations: \n"
          "mysqrt value is: {} \n"
          "math lib sqrt value is: {} \n"
          "This is a {} percent error.".format(num,iterations,my_sqrt,math_sqrt,result))

    return None

if __name__ == '__main__':
    sqrt_compare(10000,8)